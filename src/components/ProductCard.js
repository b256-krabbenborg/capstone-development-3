// import { useState } from 'react';
import { Button, Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';

// props or properties acts as a function parameter
/*
props = courseProp : { 
    description : "Nostrud velit dolor excepteur ullamco consectetur aliquip tempor. Consectetur occaecat laborum exercitation sint reprehenderit irure nulla mollit. Do dolore sint deserunt quis ut sunt ad nulla est consectetur culpa. Est esse dolore nisi consequat nostrud id nostrud sint sint deserunt dolore."
    id : "wdc001"
    name : "PHP - Laravel"
    onOffer : true
    price : 45000
}
*/
export default function CourseCard({courseProp}) {

	// console.log(props);
	// console.log(typeof props);
	// Object Deconstruction
	const {name, description, price, _id} = courseProp;

	// getter -> stores the value. variable 
	// setter -> it tells the computer how to store the value
	// initial Getter Value
	// const [count, setCount] = useState(0);
	// const [seats, setSeats] = useState(30);

	// function enroll() {

	// 	if (seats > 0) {

	// 		setCount (count + 1);
	// 		setSeats (seats - 1);
	// 		console.log('Enrollees: ' + count);
	// 	} else {

	// 		alert('No more seats');

	// 	}


	// }

	return(
		<Card>
	      <Card.Body>
	        <Card.Title>{name}</Card.Title>
	        <Card.Subtitle>Description:</Card.Subtitle>
	        <Card.Text>{description}</Card.Text>
			<Card.Subtitle>Price:</Card.Subtitle>
			<Card.Text>{price}</Card.Text>
	    	<Button variant = "primary" as={Link} to={`/courseView/${_id}`}>Details</Button>
	      </Card.Body>
	    </Card>
	)
}