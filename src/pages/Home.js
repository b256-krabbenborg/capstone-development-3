import React from 'react';
import Banner from '../components/Banner';

export default function Home() {
    const data = {
        title: "SkinKR",
        content: "An online K-Beauty destination where you can find the very best Korean skincare, make-up, and other cosmetics. We also provide personalized skincare advice and support. We pride ourselves in creating a safe space for anyone wishing to discover the joy of skincare. Our mission is to help you show your skin you care. Buying from Korean Skincare is like shopping with a friend. We will never judge you, we will understand your needs and strive to help you - whatever stage you are at. Thank you for allowing us to join you in your skincare journey.",
        // destination: "/courses",
        // label: "Enroll now!"
    }

    return (
        <>
            <Banner data={data}/>
            {/*<Highlights />*/}
        </>
    )
}
