<<<<<<< HEAD
import './App.css';
import { useState, useEffect } from 'react';
import { Container } from 'react-bootstrap';
/* The `BrowserRouter` component will enable us to simulate page navigation by synchronizing the shown content and the shown URL in the web browser.
The `Routes` component holds all our Route components. It selects which `Route` component to show based on the URL Endpoint. */
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import { UserProvider } from './userContext';
import AppNavBar from './components/AppNavBar';
import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';


function App() {

  // State hook for the user to store the information for a global scope
  // Initialized as an object with properties from the localStorage
  // This will be used to store the user information and will be used for validating if a user is logged in on the app or not
  const [user, setUser] = useState({
      id: null,
      isAdmin: null
    });

  // Function for clearing the localStorage when a user logout
  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect(() => {

    console.log(user)
    console.log(localStorage)
  })

  return (
    // Fragments are needed when there are two or more components, pages, or html elements present in the code
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
        {/* Self Closing Tags */}
        <AppNavBar /> 
        <Container>
          <Routes>
            {/*path is the endpoint and element is the page*/}
            <Route path="/" element={<Home />} />
            <Route path="/register" element={<Register />} />
            <Route path="/login" element={<Login />} />
            <Route path="/logout" element={<Logout />} />
          </Routes>
        </Container>
      </Router>
    </UserProvider>
  );
}

export default App;
=======
import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;
>>>>>>> ed7e19d96f9b9d227a65bbcbab9fd453af4dd6bd
